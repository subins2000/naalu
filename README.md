## Usage

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="./fix.js"></script>
```

## Edge cases

* മ്ല is not mapped. The character is "¾"
* There are some English words in the original file, these get converted to Unicode when passed to Freakenz

`<B>C&#169;n&#179;</B> &thorn; engine` -> `ഇഞ്ചിന്‍ - ലിഴശില` ->should be-> `ഇഞ്ചിന്‍ - Engine`

*