import csv

faulty = []
rowsToWrite = []

with open('../unicode.csv', 'r') as file:
  reader = csv.reader(file, delimiter=' ')

  for row in reader:
    if row[0] == "":
      continue
    
    if row[1] == "" or row[2] == "" or row[3] == "":
      faulty.append(row)
      continue

    rowsToWrite.append(["-", row[0][0], row[0], "malayalam", "", "", "", "", "", ""])
    rowsToWrite.append(["^", "", row[1], "kannada", "", "", "", "", "", "noun"])
    rowsToWrite.append(["^", "", row[2], "tamil", "", "", "", "", "", "noun"])
    rowsToWrite.append(["^", "", row[3], "telugu", "", "", "", "", "", "noun"])

  print(len(faulty), reader.line_num)
        
with open('dictpress.csv', mode='w') as file:
  writer = csv.writer(file, delimiter=',', quotechar='"')

  for row in rowsToWrite:
    writer.writerow(row)