class Relation < ApplicationRecord
  belongs_to :from_entry, class_name: "Entry", foreign_key: "from_id"
  belongs_to :to_entry, class_name: "Entry", foreign_key: "to_id"
end
