def relate(from_entry, to_entry)
  Relation.where(from_id: from_entry.id, to_id: to_entry.id).first_or_create
end

desc "Relate all lang entries together"
task relate_entries: [:environment] do
  puts "Total entries: #{Entry.malayalam.select(:id).includes(:definitions).count}"

  count = 0
  Entry.malayalam.select(:id).includes(:definitions).find_each do |entry|
    ActiveRecord::Base.transaction do
      # Malayalam - Kannada relation is already made by dictpress when importing
      relate(entry.definitions.kannada.first, entry) # Kannada - Malayalam dictionary
      relate(entry.definitions.kannada.first, entry.definitions.tamil.first) # Kannada - Tamil dictionary
      relate(entry.definitions.kannada.first, entry.definitions.telugu.first) # Kannada - Telugu dictionary

      relate(entry.definitions.tamil.first, entry)
      relate(entry.definitions.tamil.first, entry.definitions.kannada.first)
      relate(entry.definitions.tamil.first, entry.definitions.telugu.first)

      relate(entry.definitions.telugu.first, entry)
      relate(entry.definitions.telugu.first, entry.definitions.kannada.first)
      relate(entry.definitions.telugu.first, entry.definitions.tamil.first)
    end
    count += 1

    puts count if count % 100 == 0
  end
end
