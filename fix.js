$("font[color='#ffffff']").remove()

$("head").append(`
<style type="text/css">
@font-face {
  font-family: 'ML-TTKarthika';
  src: url('ML-TTKarthikaNormal.woff2') format('woff2'),
      url('ML-TTKarthikaNormal.woff') format('woff');
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
body {
  font-family: ML-TTKarthika;
}
</style>`
)

const partialFirstRowTables = []

function getColumns(table) {
  lastTd = null
  cols = []
  $(table).find("td[valign='TOP']").each(function() {
    if ($(this).find("p").length < 10) return;
    cols.push(this)
  })
  cols.sort(function(a, b) { 
    return $(a).offset().left - $(b).offset().left;
  })

  const firstRowHasText = $(cols[0]).find("p").first().text().trim() !== "" ||
    $(cols[1]).find("p").first().text().trim() !== "" ||
    $(cols[2]).find("p").first().text().trim() !== "" ||
    $(cols[3]).find("p").first().text().trim() !== ""

  const partialFirstRow = $(cols[0]).find("p").first().find("b").text().length < 2 &&
    $(cols[1]).find("p").first().find("b").text().length < 2 &&
    firstRowHasText

  if (partialFirstRow) {
    partialFirstRowTables.push($(table).attr("page"))
  }
  return cols
}

function getRows(cols) {
  const rows = []
  const colCursors = [0, 0, 0, 0]

  const getColumnValue = (colIndex) => {
    let text = $(cols[colIndex]).find("p").eq(colCursors[colIndex]).text().trim(),
        pElem;

    const firstColumnText = $(cols[0]).find("p").eq(colCursors[0]).text().trim()

    // ImWpI = കാണുക
    // Some entries are like this: അവമാനം - 'അപമാനം' കാണുക
    // with other columns empty values
    const isKaanukaEntry = firstColumnText.includes("ImWpI")

    const firstRowOfTableIsContinuationOfLastTable = partialFirstRowTables.includes(
      $(cols[0]).parents("table").attr("page")
    )

    if (isKaanukaEntry || firstRowOfTableIsContinuationOfLastTable) {
      return text
    }

    // console.log(colCursors, colIndex, text)

    while (
      (pElem = $(cols[colIndex]).find("p").eq(colCursors[colIndex])).length !== 0 &&
      (text = pElem.text().trim()) === ""
    ) {
      colCursors[colIndex]++
    }

    return text
  }

  let pElem;
  while ((pElem = $(cols[0]).find("p").eq(colCursors[0])).length !== 0) {
    const text = getColumnValue(0)

    // There are letter heading entries. Example: see above "CI-gv¯pI" (ഇകഴ്ത്തുക)
    const isLetterHeading = text.length >= 1 && text.length <= 2

    if (isLetterHeading) {
      console.log(text)
      colCursors[0]++
      continue
    }

    let row = [text, "", "", ""]
    for (let i = 1; i <= 3; i++) {
      row[i] = getColumnValue(i)
    }

    rows.push(row)

    colCursors[0]++
    colCursors[1]++
    colCursors[2]++
    colCursors[3]++
  }

  return rows
}

function pagemakerTextCleanup(text) {
  return text
    .replace(/\n/g, "")
    .replace("&quot;", "'")
    .replace("¾", "മ്ല") // This is missed out in Freakenz
}

function convertToCsv(rows) {
  var makeRow = function(row) {
    return row.map(elem => 
      `"${pagemakerTextCleanup(elem.toString()).replace(/"/g, '""')}"`
    ).join(" ");
  }
  return rows.map(row => makeRow(row)).join("\n");
}

function makeOutput(text) { 
  $("body").prepend(`<textarea cols="200" rows="10">${text}</textarea>`)
}

function doForAllTables() {
  const rows = []
  $("table").each(function() {
    const tableRows = getRows(getColumns(this))

    if (partialFirstRowTables.includes($(this).attr("page"))) {
      for (let i = 0; i <= 3; i++) {
        rows[rows.length - 1][i] += tableRows[0][i]
      }
      rows.push(...tableRows.slice(1))
    } else {
      rows.push(...tableRows)
    }
  })

  console.log(partialFirstRowTables)
  makeOutput(convertToCsv(rows))
}

$("table").each(function() {
  const pageNumber = this.previousSibling.previousSibling.textContent.match(/page\s(.*?)\s/)[1]
  $(this).attr("page", pageNumber)
})

doForAllTables()
// makeOutput(convertToCsv(getRows(getColumns($("table[page='5']").last()))))